terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "demo-devops"

    workspaces {
      name = "aws"
    }
  }
}